import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Active from './Active';
function Movie() {
    return (
        <div>
            <Router>
                 <h1>Movie Category</h1>
                <ul>
                    <li>
                        <Link to="/Video/Movie/adventure">Adventure</Link>
                    </li>
                    <li>
                        <Link to="/Video/Movie/comedy">Comedy</Link>
                    </li>
                    <li>
                        <Link to="/Video/Movie/crime">Crime</Link>
                    </li>
                    <li>
                        <Link to="/Video/Movie/documentary">Documentary</Link>
                    </li>
                </ul>
                <Switch>
                <Route path="/Video/Movie/:name" component={Active} />
                <h1>Please Select A Topic</h1>
                </Switch>
            </Router>
        </div>
    )
}

export default Movie
