import React from "react";
import { Card, Button } from "react-bootstrap";
import {Link} from 'react-router-dom'

function CardItem(props) {
  // let a= props.id;
  
  return (
  
    <div className="col-md-4" key={props.id}>
      <Card style={{ width: "20rem" }}>
        <Card.Img
          variant="top"
          src="https://exp.gg/wp-content/uploads/2018/10/asdasdasd1-768x543.png"
        />
        <Card.Body>
          <Card.Title>{props.name}</Card.Title>
          <Card.Text>{props.des}</Card.Text>
          <Link to={`/Post/${props.id}`}>
            <Button variant="primary">see more</Button>
          </Link>
        </Card.Body>
      </Card>
    </div>
  );
}

export default CardItem;
