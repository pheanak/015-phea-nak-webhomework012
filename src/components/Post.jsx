import React from 'react'

function Post(props) {
    let idSelect = props.match.params.id
    return (
        <div>
            
            <p>This is component from post {idSelect}</p>
            
        </div>
    )
}

export default Post
