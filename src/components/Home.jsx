import React, { Component } from "react";
import CardItem from "./CardItem";

export default class Home extends Component {
  constructor() {
    super();
    this.state = {
      Users: [
        {
          id: "1",
          name: "Minotur",
          des:
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
          img:
            "https://exp.gg/wp-content/uploads/2018/10/asdasdasd1-768x543.png",
        },
        {
          id: "2",
          name: "Lesley",
          des:
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
          img:
            "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510__340.jpg",
        },
        {
          id: "3",
          name: "Helcut",
          des:
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
          img:
            "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510__340.jpg",
        },
     
      ],
    };
  }
  render() {
    return (
      <div className="container">
        <div className="row">
          {this.state.Users.map((item, index) => (
            <CardItem key={item.id} name={item.name} des={item.des} id={item.id} />
          ))}
        </div>
      </div>
    );
  }
}
